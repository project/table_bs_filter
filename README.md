# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Bootstrap any table.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/table_bs_filter>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/table_bs_filter>


## REQUIREMENTS

Drupal core "editor" and "filter" modules.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > Content > Formats
    3. Choose your text format and enable "Add Bootstrap Class to any tables"
       filter
    4. Set checkboxes for settings you need.
    5. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
