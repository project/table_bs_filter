<?php

namespace Drupal\table_bs_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

define('TABLE_BS_FILTER_REGEX', '/\<table.*?\>/s');
define('TABLE_BS_FILTER_CELLS_REGEX', '/\<[tr|td|th].*?\>/s');
define('TABLE_BS_FILTER_END_REGEX', '/\<\/table.*?\>/s');

/**
 * Add Bootstrap Class to any tables.
 *
 * @Filter(
 *   id = "table_bs_filter",
 *   title = @Translation("Add Bootstrap Class to any tables"),
 *   description = @Translation("This filter will add table table-stripped table-hover table-responsive to any tables."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class TableBSFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['remove_width_height'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove Width & Height From Cells'),
      '#default_value' => $this->settings['remove_width_height'] ?? FALSE,
      '#description' => $this->t('This option will cleared width and height from cells.'),
    ];
    $form['table_bordered'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add border to table'),
      '#default_value' => $this->settings['table_bordered'] ?? FALSE,
      '#description' => $this->t('This option will wrap the table in a border.'),
    ];
    $form['table_condensed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Condensed table'),
      '#default_value' => $this->settings['table_condensed'] ?? FALSE,
      '#description' => $this->t('This option tables more compact by cutting cell padding in half.'),
    ];
    $form['table_row_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add hover to table rows'),
      '#default_value' => $this->settings['table_row_hover'] ?? FALSE,
      '#description' => $this->t('This option adds a hover effect on table rows.'),
    ];
    $form['table_striping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add alternate row striping'),
      '#default_value' => $this->settings['table_striping'] ?? FALSE,
      '#description' => $this->t('This option will make every second row a different colour.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = preg_replace_callback(TABLE_BS_FILTER_REGEX, [
      &$this,
      'tableBsFilterReplace',
    ], $text);
    if ($this->settings['remove_width_height']) {
      $text = preg_replace_callback(TABLE_BS_FILTER_CELLS_REGEX, [
        &$this,
        'tableBsFilterRemoveWidthHeight',
      ], $text);
    }
    $text = preg_replace_callback(TABLE_BS_FILTER_END_REGEX, [
      &$this,
      'tableBsFilterEndReplace',
    ], $text);

    return new FilterProcessResult($text);
  }

  /**
   * Callback to convert a table to bootstrap table.
   *
   * @param string $match
   *   Takes a match of tag code.
   *
   * @return string
   *   The HTML markup representation of the tag, or an empty string on failure.
   */
  private function tableBsFilterReplace($match) {
    $table_id = '';
    $table_classes = 'table';
    $table_styles = '';
    $table_direction = '';
    $settings_bootstrap_classes = '';

    if (preg_match('/id="(.+)"/', $match[0], $id)) {
      $table_id = ' id="' . $id[1] . '"';
    }
    if (preg_match('/class="(.+)"/U', $match[0], $classes)) {
      $table_classes .= ' ' . $classes[1];
    }
    if (preg_match('/style="(.+)"/', $match[0], $styles)) {
      $table_styles = 'style="' . $styles[1] . '"';
      if ($this->settings['remove_width_height']) {
        $table_styles = preg_replace('/width.+;/', '', $table_styles);
      }
    }
    if (preg_match('/dir="([^"]+)"/', $match[0], $direction)) {
      $table_direction = 'dir="' . $direction[1] . '"';
    }

    if ($this->settings['table_bordered']) {
      $settings_bootstrap_classes .= " table-bordered";
    }
    else {
      $settings_bootstrap_classes .= " table-borderless";
    }
    if ($this->settings['table_condensed']) {
      $settings_bootstrap_classes .= " table-condensed";
    }
    if ($this->settings['table_row_hover']) {
      $settings_bootstrap_classes .= " table-hover";
    }
    if ($this->settings['table_striping']) {
      $settings_bootstrap_classes .= " table-striped";
    }

    return '<div class="table-responsive" ' . $table_direction . '><table class="' . ltrim($table_classes) . rtrim($settings_bootstrap_classes) . '"' . $table_id . $table_styles . $table_direction . '>';
  }

  /**
   * Callback to convert a table to bootstrap table & remove cells width/height.
   *
   * @param string $match
   *   Takes a match of tag code.
   *
   * @return string
   *   The HTML markup representation of the tag, or an empty string on failure.
   */
  private function tableBsFilterRemoveWidthHeight($match) {
    $tbody = $match[0];
    $tbody = preg_replace('/width.+;/', '', $tbody);
    $tbody = preg_replace('/height.+;/', '', $tbody);
    $tbody = preg_replace('/width="(.+)"/', '', $tbody);
    $tbody = preg_replace('/height="(.+)"/', '', $tbody);
    return $tbody;
  }

  /**
   * Callback to convert a table to bootstrap table.
   *
   * @param string $match
   *   Takes a match of tag code.
   *
   * @return string
   *   The HTML markup representation of the tag, or an empty string on failure.
   */
  private function tableBsFilterEndReplace($match) {
    return '</table></div>';
  }

}
