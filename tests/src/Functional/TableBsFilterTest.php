<?php

namespace Drupal\Tests\table_bs_filter\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the table_bs_filter module.
 *
 * @group table_bs_filter
 */
class TableBsFilterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'table_bs_filter',
    'node',
    'editor',
    'filter',
  ];

  /**
   * Test Node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'page']);

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [
        'table_bs_filter' => [
          'status' => 1,
          'settings' => [
            'remove_width_height' => FALSE,
            'table_bordered' => TRUE,
            'table_condensed' => TRUE,
            'table_row_hover' => TRUE,
            'table_striping' => TRUE,
          ],
        ],
      ],
    ])->save();
  }

  /**
   * Test the table_bs_filter filters.
   */
  public function testTableBsFilters(): void {
    $this->node = $this->createNode([
      'body' => [
        'value' => '
          <table cellspacing="1" class="demo-class" cellpadding="1" border="1">
              <tbody>
                  <tr>
                      <td><strong>Title 1</strong></td>
                      <td><strong>Title 2</strong></td>
                  </tr>
                  <tr>
                      <td>Cell 1</td>
                      <td>1</td>
                  </tr>
                  <tr>
                      <td>Cell 2</td>
                      <td>2</td>
                  </tr>
                  <tr>
                      <td>Cell 3</td>
                      <td>3</td>
                  </tr>
              </tbody>
          </table>',
        'format' => 'full_html',
      ],
    ]);

    $this->drupalGet('node/' . $this->node->id());
    $page = $this->getSession()->getPage();
    $table = $page->find('css', 'table');
    $this->assertEquals('table demo-class table-bordered table-condensed table-hover table-striped', $table->getAttribute('class'));
  }

}
